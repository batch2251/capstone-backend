const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const cors = require("cors");
const publicRoute = require("./routes/publicRoutes")
const userRoute = require("./routes/userRoutes")
const adminRoute = require("./routes/adminRoutes")
const productRoutes = require("./routes/productRoutes")
const cartRoutes = require("./routes/cartRoutes")
const orderRoutes = require("./routes/orderRoutes")
const viewRoutes = require("./routes/htmlRoutes")
const app = express();
const port = 8080;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.set('view engine', 'ejs');

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${process.env.M_USERNAME}:${process.env.M_PASSWORD}@cluster0.soshf7e.mongodb.net/listahan_app?retryWrites=true&w=majority`,
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
    
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log("Connected to MongoDB!"));

app.use("/admin", adminRoute);
app.use("/api/users", userRoute);
app.use("/api/products", productRoutes);
app.use("/api", publicRoute);
app.use("/api/cart", cartRoutes);
app.use("/api/order", orderRoutes);
app.use("/", viewRoutes);


// app.listen(port, "192.168.5.108");

app.listen(port, () => console.log (`Now listening to port ${port}...`));