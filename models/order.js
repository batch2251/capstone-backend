const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const orderSchema = new mongoose.Schema ({
	
	userId : {
		type : mongoose.Schema.Types.ObjectId,
		required : [true, "User ID is required"]
	},
	items : [
		{
			productId : {
				type : mongoose.Schema.Types.ObjectId,
			},
			name: {
				type: String,
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			price : {
				type: Number,
				required: [true, "Item Price is required"]
			},
			subtotal : {
				type: Number,
				required: [true, "products[].subTotal is required"]
			}		
		}
	],
	subtotal: {
		type: Number,
		required: [true, "totalAmount is required"]
	},
	// shippingFee: {
	// 	type: Number,
	// 	default: parseInt(process.env.SHIPPING)
	// },
	// tax: {
	// 	type: Number,
	// 	default: parseInt(process.env.TAX)
	// },
	// overAllTotal: {
	// 	type: Number,
	// 	required: [true, "overAllTotal is required"]
	// },
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	status: {
		type: String,
		required: [true, "status is required"],
		default: "Active"
	},
	isArchived: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("Order", orderSchema);