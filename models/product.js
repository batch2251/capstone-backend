const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	
	name : {
		type : String,
		required : [true, "Product is required"]
	},
	imageUrl : {
		type : String,
		default: null
	},	
	shortDescription : {
		type : String
	},	
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : !!true
	},
	featured : {
		type : Boolean,
		default : false
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	category : {
      type: Array,
      'default': ['Electronics']
    },
	keywords : {
      type: String
    }
})

module.exports = mongoose.model("Product", productSchema);