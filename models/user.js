const mongoose = require("mongoose");
const userSchema = new mongoose.Schema ({

	firstName: { 
		type: String,
		required: [true, "First name is required"]
	},
	lastName: { 
		type: String,
		required: [true, "Last name is required"]
	},
	email: { 
		type: String,
		required: [true, "Email is required"]
	},
	password: { 
		type: String,
		required: [true, "Password is required"]
	},
	role: {
		type: Number,
		default: 2
	},
	mobileNo: {
		type: String
	},
	address: {
		street1: {
			type: String,
			required: [true, "Address is required"]
		},
		street2: {
			type: String,
			default: null,
		},
		city: {
			type: String,
			required: [true, "city is required"]
		},
		province: {
			type: String,
			required: [true, "province is required"]
		},
		country: {
			type: String,
			required: [true, "country is required"]
		},
		postalCode: {
			type: String,
			required: [true, "postal code is required"]
		}
	},
	active : {
		type : Boolean,
		default : true
	},	
	createdOn : {
		type : Date,
		default : new Date()
	},			
});

module.exports = mongoose.model("User", userSchema);