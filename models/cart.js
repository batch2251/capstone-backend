const mongoose = require("mongoose");
const cartSchema = new mongoose.Schema({
	userId : {
		type : mongoose.Schema.Types.ObjectId,
		required : [true, "User ID is required"]
	},
	items : [
		{
			productId : {
				type : mongoose.Schema.Types.ObjectId
			},
			name: { type: String },			
			quantity: {
				type: String,
				required: [true, "Quantity is required"],
				default: 1
			},
			price: {
				type: Number,	
			},
			bought: {
				type: Boolean,
				default: false
			},
			disableName: {
				type: Boolean,
				default: false
			},
			disablePrice: {
				type: Boolean,
				default: false
			},
			index: {
				type: String,
			},		
		}
	],
	createdOn: {
		type: Date,
		default: new Date()
	},
	status: {
		type: String,
		required: [true, "status is required"],
		default: "Active"
	},
	isArchived: {
		type: Boolean,
		default: false
	}	
})

module.exports = mongoose.model("Cart", cartSchema);