const Cart = require("../models/cart");
const auth = require("../auth");
const orderController = require("./orderController")
const mongoose = require("mongoose");

module.exports.cartItems = async (data) => {
	let userHasCart = await Cart.aggregate([
      { $match: { 'userId': data.currentUserId }  },        
      {
        '$lookup': {
          'let': { 'userObjId': {'$toObjectId': '$userId'} }, 
          'from': 'users', 
          'pipeline': [{ '$match': { '$expr': {'$eq': ['$_id', '$$userObjId']} } }], 
          'as': 'user'
        }
      }, 
      { '$unwind': {'path': '$user'} }, 
      { '$unwind': {'path': '$products'} }, 
      {
        '$lookup': {
          'from': 'products', 
          'let': {
            'itemId': {'$toObjectId': '$products.productId'}, 
            'items': '$products'
          }, 
          'pipeline': [
            { '$match': { '$expr': {'$eq': ['$_id', '$$itemId']} } }, 
            { '$replaceRoot': {'newRoot': {'$mergeObjects': ['$$items', '$$ROOT']}} }
          ], 
          'as': 'items'
        }
      }, 
      {
        '$unwind': { 'path': '$items' }
      }, 
      {
        '$project': {
        	'_id': 1,
        	'tax': 1,
        	'shippingFee': 1,
					'user.id': '$userId',
					'user.firstName': '$user.firstName', 
					'user.lastName': '$user.lastName', 
					'user.email': '$user.email', 
					'products.productId': '$products.productId',
					'products.quantity': 1, 
					'products.name': '$items.name', 
					'products.price': '$items.price',
					'products.subtotal': {'$multiply': ['$items.price', '$products.quantity'] }
        }
      },
      {
      	'$group': {
				  '_id': '$_id',
				  'tax': { '$first': '$tax'},
				  'shipping': { '$first': '$shippingFee'},
				  'user': { '$first': '$user'},
				  'products': { '$push': '$products'},
				  'total': {'$sum': '$products.subtotal'}	  
				}
      }
      ]).then(userCart => {
        return ( !userCart || userCart.length < 1 ) ? false : userCart
    });

      if (userHasCart) {
      		let data = {
						result: userHasCart
      		}
          return data
      } else {
  				return { error: 'Your cart is empty' }	
      }
}

module.exports.cartAdd = async (data) => {
	const item = { productId: data.data.productId, quantity: data.data.quantity }
	let userHasCart = await Cart.findOne({ userId: data.currentUserId }).then(userCart => {
		return ( !userCart || userCart.length < 1 ) ? false : userCart
	});

	if (userHasCart) {
		// if Cart collection has record of the current user ... append the new product
		newProduct = userHasCart.products
		
			// check if the product is already in the cart
			let alreadyAdded = userHasCart.products.map( (v, k) => {
				return (v.productId === item.productId) ? k : null
			}).filter(Number.isInteger);

			if ( alreadyAdded.length > 0 ) {
				// if it is already added.. add the quantity to that product
				newProduct[alreadyAdded].quantity += item.quantity
			} else {
				// else append the product
				newProduct.push(item)
			}
			//end checking

		let updatedCart = await Cart.findByIdAndUpdate(userHasCart.id, { products : newProduct }, {new: true}).then( (cart, error) => {
			return (error) ? false : cart
		});		

		return (updatedCart) ? { message: 'Added to Cart!', data: updatedCart } : { error: 'Failed to update Cart' }

	} else {
		// if Cart collection has no record of the current user ... create new cart
		let newCart = new Cart({
			userId : data.currentUserId,
			products : item,
			shippingFee: process.env.SHIPPING,
			tax:  process.env.TAX,
		});

		return newCart.save().then( (cart, error) => {
			return (error) ? false : { message: 'Added to Cart!', data: cart }
		});
	}
}

module.exports.cartProductQuantity = async (data) => {
	//console.log(data)
	let userHasCart = await Cart.findOne({ userId: data.currentUserId }).then(userCart => {
		return ( !userCart || userCart.length < 1 ) ? false : userCart
	});

	if (userHasCart) {
		newProduct = userHasCart.products

		let productKey = userHasCart.products.map( (v, k) => {
			return (v.productId === data.cartProductsId) ? k : null
		}).filter(Number.isInteger);

		if (productKey.length == 0) {
			return Promise.resolve({ error: 'No such product in you cart!' })
		}

		newProduct[productKey].quantity = data.quantity

		let updatedCart = await Cart.findByIdAndUpdate(userHasCart.id, { products : newProduct }, {new: true}).then( (cart, error) => {
			return (error) ? false : cart
		})

		return (updatedCart) ? { message: 'Cart Updated!', data: updatedCart } : { error: 'Failed to update Cart' }

	} else {
		return { error: "You don't have a cart yet!" }
	}
}

module.exports.cartProductDelete = async (data) => {
	//console.log(data)
	let userHasCart = await Cart.findOne({ userId: data.currentUserId }).then(userCart => {
		return ( !userCart || userCart.length < 1 ) ? false : userCart
	});

	if (userHasCart) {
		newProduct = userHasCart.products

		let productKey = userHasCart.products.map( (v, k) => {
			return (v.productId === data.cartProductsId) ? k : null
		}).filter(Number.isInteger);

		if (productKey.length == 0) {
			return Promise.resolve({ error: 'No such product in you cart!' })
		} else {
			newProduct.splice(productKey, 1)			
		}

		let updatedCart = await Cart.findByIdAndUpdate(userHasCart.id, { products : newProduct }, {new: true}).then( (cart, error) => {
			return (error) ? false : cart
		})

		return (updatedCart) ? { message: 'Cart Updated!', data: updatedCart } : { error: 'Failed to update Cart' }

	} else {
		return { error: "You don't have a cart yet!" }
	}
}

module.exports.cartCheckout = async (data) => {
	let cartItems = await this.cartItems(data) // reuse module.exports.cartItems function

	if (cartItems) {
		
		let checkoutItems = {
				// complicated structure due to it needs to simulate posting of object from postman/admin checkout
				currentUserId: cartItems.result[0].user.id, 				
				data: {
			    items: cartItems.result[0].products,
			    subtotal: cartItems.result[0].total,					
		    	overAllTotal: parseInt(cartItems.result[0].total) + parseInt(process.env.SHIPPING) + (cartItems.result[0].total * parseInt(process.env.TAX) / 100),
		    	//overAllTotal: cartItems.result[0].total,
			    shippingFee: process.env.SHIPPING,
			    tax: process.env.TAX
				}
		}


		// reuse /controllers/orderController function
		let createdOrder = await orderController.createOrder(checkoutItems) 
		// delete cart items
		let removedCartItems = await Cart.findByIdAndUpdate(cartItems.result[0]._id, { products : [] }, {new: true}).then( (cart, error) => {
			return (error) ? false : cart
		})

		return (createdOrder && removedCartItems) ? createdOrder : { error: 'Failed to create order!' }

	} else {
		return { error: "You don't have a cart yet!" }
	}
}

module.exports.cartCreate = async(data) => {
		//console.log(data.data.items.data)
		let newCart = new Cart({
			userId : mongoose.Types.ObjectId(data.userId),
			items : data.data.items.data,
			createdOn: new Date(),
		});

		return newCart.save().then( (cart, error) => {
			return (error) ? { error: error } : { message: 'Success in saving data!', data: cart }
		});	
}