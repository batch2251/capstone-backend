const dotenv = require("dotenv").config();
const productController = require("../controllers/productController")
const publicController = require("../controllers/publicController")
const cartController = require("../controllers/cartController")
const userController = require("../controllers/userController")
const auth = require("../auth")

const homeView = (req, res) => { res.render("home", {}); }
const dashboardAdmin = (req, res) => { res.render("dashboard-admin", {}); }

const registerView = (req, res) => { res.render("register", {}); }
const registerSuccess = (req, res) => { res.render("register-success", {}); }
const registerPost = async (req, res) => { 
    let endpoint = await publicController.registerUser(req.body).then( result => { 
        return result
    });    

    if (endpoint) {
        if ( 'error' in endpoint ) {
            res.render("register", { error: endpoint.error })    
        } else {
            res.redirect('/register-success')
        }
    }    
}

const loginView = (req, res) => { res.render("login", {}); }
const loginPost = async (req, res) => { 
    let endpoint = await publicController.loginUser(req.body, res).then( result => { 
        return result
    });    

    if (endpoint) {
        if( 'error' in endpoint ) {
            res.render('login', { error: endpoint.error })
        } else {
          res.cookie(process.env.COOKIE_NAME, endpoint.access)
          if (endpoint.isAdmin) {
            res.redirect('/admin')
          } else {
            res.redirect('/products')
          }
        }    
    }    
}

const errorLoginView = (req, res) => { res.render("errorLogin", {}); }

const productsView = async (req, res) => { 
    let endpoint = await productController.productList(req, res).then( result => { 
            return result
    });    

    if (endpoint) {
        res.render("products", { data: endpoint }); 
    }
}

const productDetailsView = async (req, res) => {
    let endpoint = await productController.product(req, res).then( result => { 
        return result
    });    

    if (endpoint) {
        res.render("productDetails", { data: endpoint }); 
    }    
}

const ordersView = async (req, res) => { 
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)
    const data = {
        data: req.body,
        params: auth.decode(token).id,
        isAdmin: auth.decode(token).isAdmin,
        currentUserId: auth.decode(token).id
    }

    let endpoint = await userController.myOrders(data).then( result => { 
        return result
    });    

    if (endpoint) {
        res.render("orders", { data: endpoint }); 
    }
}

const ordersSingleView = async (req, res) => { 
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)
    const data = {
        data: req.body,
        params: req.params.id,
        isAdmin: auth.decode(token).isAdmin,
        currentUserId: auth.decode(token).id
    }

    let endpoint = await userController.myOrderDetails(data).then( result => { 
        return result
    });    

    if (endpoint) {
        res.render("orderDetails", { data: endpoint }); 
    }
}


const cartView = async (req, res) => {
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)
    const data = {
        data: req.body,
        currentUserId: auth.decode(token).id
    }

    let endpoint = await cartController.cartItems(data).then( result => { 
        return result
    });    

    if (endpoint) {
        let data = ( 'error' in endpoint ) ? { error: endpoint.error } : { data: endpoint }
        res.render("cart", data)              
    }    
}

const cartAdd = async (req, res) => {
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)    
    const data = {
        data: { productId: req.params.id, quantity: 1 },
        currentUserId: auth.decode(token).id
    }

    let endpoint = await cartController.cartAdd(data).then( result => { 
        return result
    });    

    if (endpoint) {
        let data = ( 'error' in endpoint ) ? { error: endpoint.error } : { data: endpoint }
        res.redirect('/cart')            
    }    
}

const cartUpdateQuantity = async (req, res) => {
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)    

    const data = {
        currentUserId: auth.decode(token).id,
        cartProductsId: req.params.id,
        quantity: req.query.quantity        
    }

    let endpoint = await cartController.cartProductQuantity(data).then( result => { 
        return result
    });    

    if (endpoint) {
        let data = ( 'error' in endpoint ) ? { error: endpoint.error } : { data: endpoint }
        res.redirect('/cart')            
    }    
}

const cartRemove = async (req, res) => {
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)    

    const data = {
        currentUserId: auth.decode(token).id,
        cartProductsId: req.params.id      
    }

    let endpoint = await cartController.cartProductDelete(data).then( result => { 
        return result
    });    

    if (endpoint) {
        let data = ( 'error' in endpoint ) ? { error: endpoint.error } : { data: endpoint }
        res.redirect('/cart')            
    }    
}

const cartCheckout = async (req, res) => {
    let token = 'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length)    

    const data = {
        currentUserId: auth.decode(token).id
    }

    let endpoint = await cartController.cartCheckout(data).then( result => { 
        return result
    });    

    if (endpoint) {
        let data = ( 'error' in endpoint ) ? { error: endpoint.error } : { data: endpoint }
        res.redirect('/cart')            
    }    
}

module.exports =  {
    homeView,
    dashboardAdmin,
    registerView, registerPost, registerSuccess,
    loginView, loginPost,
    errorLoginView,    
    productsView,productDetailsView,
    ordersView,ordersSingleView,
    cartView, cartAdd, cartUpdateQuantity, cartRemove, cartCheckout
};