const User = require("../models/user");
const Order = require("../models/order");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const mongoose = require("mongoose");

module.exports.userProfile = (data, res) => {

	return User.findById(data.currentUserId).then(result => {
		return result;
	});
};

module.exports.updateProfile = (data, res) => {
	let newUser = {
		firstName: data.data.firstName,
		lastName: data.data.lastName,
		email: data.data.email,
		mobileNo: data.data.mobileNo,
		address: {
			street1: data.data.address.street1,
			street2: data.data.address.street2,
			city: data.data.address.city,
			province: data.data.address.province,
			country: data.data.address.country,
			postalCode: data.data.address.postalCode
		},
	};
return User.findByIdAndUpdate(data.currentUserId, newUser).then((data, error) => {
	if (error) {
		return false;
	} else {
		return {
			message: "Account updated successfully"
		}
	};
});
};

module.exports.deleteProfile = (data, res) => {
	return Promise.resolve({message: "Account successfully deleted kunwari lang"})
	// return User.findByIdAndRemove(data.currentUserId).then ((result, err) => {
	// 	if (err) {
	// 		console.log(err);
	// 		return false;
	// 	} else {
	// 		return {
	// 		message: "Account successfully deleted kunwari lang"
	// 	   }
	// 	}
	// });
};

module.exports.myOrders = (data, res) => {
	let date = new Date()

	let month = (data.month) ? ('0' + [parseInt(data.month) + 1]).slice(-2) : ('0' + [parseInt(date.getMonth()) + 1]).slice(-2)
	let year = (data.year) ? data.year : date.getFullYear()
	let date_start = `${year}-${month}-01`
	let date_end = `${year}-${month}-31`	

	return Order.find(
			{
			    $and: [
			        {'userId': data.params },
			        {
					      'purchasedOn': {
					        '$gte': new Date(`${date_start}T00:00:00.000Z`),
					        '$lt': new Date(`${date_end}T23:59:59.999Z`)
					      }
			        }
			    ]
			}		
		).sort({'purchasedOn': 1}).then(result => {
		return result;
	})		
};

module.exports.myOrderDetails = (data, res) => {
	let date = new Date()
	let month = (data.month) ? ('0' + [parseInt(data.month) + 1]).slice(-2) : ('0' + [parseInt(date.getMonth()) + 1]).slice(-2)
	let year = (data.year) ? data.year : date.getFullYear()
	let date_start = `${year}-${month}-01`
	let date_end = `${year}-${month}-31`	

	return Order.aggregate([
		 {
		  "$match":{
		    "$and":[
		      { '_id': mongoose.Types.ObjectId(data.params) },
	        {
			      'purchasedOn': {
			        '$gte': new Date(`${date_start}T00:00:00.000Z`),
			        '$lt': new Date(`${date_end}T23:59:59.999Z`)
			      }
	        }		      
		    ]
		 }
		}, 
	  { '$sort': {'purchasedOn': -1} }, 
	  {
	    '$lookup': {
	      'from': 'users', 
	      'localField': 'userId', 
	      'foreignField': '_id', 
	      'as': 'user'
	    }
	  }, {
	    '$unwind': {
	      'path': '$user'
	    }
	  }
	 //  OLD AGGREGGATE			
	 //  { '$match': { '_id': mongoose.Types.ObjectId(data.params) } }, 
	 //  { '$unwind': { 'path': '$items'} }, 
	 //  {
	 //    '$lookup': {
		//   'let': { 'userObjId': { '$toObjectId': '$userId'} }, 
		//   'from': 'users', 
		//   'pipeline': [ {'$match': {'$expr': {'$eq': ['$_id', '$$userObjId']}} } ], 
		//   'as': 'user'
		// }
	 //  }, 
	 //  { '$unwind': { 'path': '$user'}}, 
	 //  { '$unwind': { 'path': '$items'}}, 
	 //  {
	 //    '$lookup': {
		//   'from': 'products', 
		//   'let': {
		//     'zitemId': {'$toObjectId': '$items.productId'}, 
		//     'zitems': '$items'
		//   }, 
		//   'pipeline': [
		//     {'$match': {'$expr': {'$eq': ['$_id', '$$zitemId']}}}, 
		//     {'$replaceRoot': {'newRoot': {'$mergeObjects': ['$$zitems', '$$ROOT']}}}
		//   ], 
		//   'as': 'zitems'
		// }
	 //  }, 	  
	 //  { '$unwind': { 'path': '$zitems'}}, 
	 //  {
	 //    '$project': {
	 //      '_id': 1, 
	 //      'user.id': '$userId', 
	 //      'user.firstName': '$user.firstName', 
	 //      'user.lastName': '$user.lastName', 
	 //      'user.email': '$user.email', 
	 //      'items.productId': '$items.productId', 
	 //      'items.quantity': 1, 
	 //      'items.name': '$zitems.name', 
	 //      'items.price': '$zitems.price', 
	 //      'items.subtotal': {'$multiply': ['$zitems.price', '$items.quantity']}, 
	 //      'totals.subtotal': '$subtotal', 
	 //      'totals.shippingFee': '$shippingFee',
	 //      'totals.tax': '$tax',
	 //      'totals.overAllTotal': '$overAllTotal',
	 //      'purchasedOn': 1,
	 //      'status': 1
	 //    }
	 //  }, 
	 //  {
	 //    '$group': {
	 //      '_id': '$_id', 
	 //      'user': { '$first': '$user' }, 
	 //      'items': { '$push': '$items' }, 
	 //      'totals': { '$first': '$totals'},
	 //      'purchasedOn': { '$first': '$purchasedOn'},
	 //      'status': { '$first': '$status'}
		// }
	 //  }  
	]).then( details => {
		return details
	})
};

module.exports.myOrderDetailsByItem = (data, res) => {
	let date = new Date()
	let month = (data.month) ? ('0' + [parseInt(data.month) + 1]).slice(-2) : ('0' + [parseInt(date.getMonth()) + 1]).slice(-2)
	let year = (data.year) ? data.year : date.getFullYear()
	let date_start = `${year}-${month}-01`
	let date_end = `${year}-${month}-31`	


	return Order.aggregate([
	  // { '$match': { 'userId': mongoose.Types.ObjectId(data.params) } }, 
		 {
		  "$match":{
		    "$and":[
		      { 'userId': mongoose.Types.ObjectId(data.params) },
	        {
			      'purchasedOn': {
			        '$gte': new Date(`${date_start}T00:00:00.000Z`),
			        '$lt': new Date(`${date_end}T23:59:59.999Z`)
			      }
	        }		      
		    ]
		 }
		}, 	  
	  {
	    '$lookup': {
	      'from': 'users', 
	      'localField': 'userId', 
	      'foreignField': '_id', 
	      'as': 'userInfo'
	    }
	  }, 
	  { '$unwind': {'path': '$userInfo'} }, 
	  {
	    '$project': {
	      'order.user.fname': '$userInfo.firstName', 
	      'order.user.lname': '$userInfo.lastName', 
	      'order.user.id': '$userId', 
	      'order.items': '$items'
	    }
	  }, 
	  {'$unwind': {'path': '$order.items'} }, 
	  {
	    '$project': {
	      'order.items.price': 0, 
	      'order.items.quantity': 0
	    }
	  }, 
	  {
	    '$group': {
	      '_id': '$order.items.name', 
	      'expense': {
	        '$sum': '$order.items.subtotal'
	      }
	    }
	  }, 
	  {
	    '$project': {
	      '_id': 0, 
	      'item.name': '$_id', 
	      'item.expense': '$expense'
	    }
	  }, 
	  {
	    '$group': {
	      '_id': 'results', 
	      'result': {
	        '$push': '$item'
	      }
	    }
	  }
	]).then( details => {
		return details
	})	
}