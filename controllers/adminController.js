const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const dotenv = require("dotenv").config();

// ADMIN PANEL USER INFO RELATED

module.exports.userList = async (data, res) => {

	let pagination = (data.page) ? (parseInt(data.page) - 1) * process.env.PER_PAGE : 0

	if (data.role === 0) {

		let userCount = await User.find().then(result => {	
			return result.length;
		});

		let paginatedUsers = await User.find({}, '_id active firstName lastName email isAdmin mobileNo address createdOn').sort({createdOn: 'descending'}).skip(pagination).limit(process.env.PER_PAGE)
		.then(result => {	
			return result
		});		

		if(userCount && paginatedUsers) {
			return { 
	    	result: paginatedUsers, 
	    	number_of_users: userCount, 
	    	current_page: (data.page) ? parseInt(data.page) : 1,
	    	total_pages: Math.ceil(userCount/parseInt(process.env.PER_PAGE)) 
			}
		} else { 
			return { error: 'Server Error!'} 
		}	



	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};


module.exports.addUser = (data, res) => {
	if (data.role === 0) {
		return User.find({email: data.data.email }).then( result => {	
			if (result.length > 0) {
				return Promise.resolve({ error: `Email: ${data.data.email} already exists in our database!` })
			} else {
				// start saving on mongodb
				let newUser = new User({
					firstName: data.data.firstName,
					lastName: data.data.lastName,
					email: data.data.email,
					mobileNo: data.data.mobileNo,
					password: bcrypt.hashSync(data.data.password, 10),
					address: {
						street1: data.data.address.street1,
						street2: data.data.address.street2,
						city: data.data.address.city,
						province: data.data.address.province,
						country: data.data.address.country,
						postalCode: data.data.address.postalCode
					},
					isAdmin: data.data.role === 0
				});

				return newUser.save().then((user, error) => {
					if (error){
						return {error: "failed saving"};
					} else {
						return user
					}
				}).catch( error => {
					return Promise.resolve(error)
				});						
				// end saving on mongodb	
			}
		}).catch( error => {
			return Promise.resolve(error)
		});
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.userProfile = (data, res) => {
	//console.log(data)
	if (data.role === 0) {
		return User.findById(data.params, '_id active firstName lastName email role mobileNo address createdOn').then(result => {
			return result;
		})		
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.userUpdate = async (data, res) => {
	if (data.role === 0) {
		let newContent = {
			firstName: data.data.firstName,
			lastName: data.data.lastName,
			email: data.data.email,
			mobileNo: data.data.mobileNo,
			address: {
				street1: data.data.address.street1,
				street2: data.data.address.street2,
				city: data.data.address.city,
				province: data.data.address.province,
				country: data.data.address.country,
				postalCode: data.data.address.postalCode,
			},
			role: data.data.role,
			active: data.data.active
		};

		let updated = await User.findByIdAndUpdate(data.params, newContent, { new: true, select: '_id active firstName lastName email role mobileNo address createdOn' })
		.then((result, error) => {
			return (error) ? false : result
		});

		return (updated) ? { message: "User updated successfully", data: updated } : { error: "Failed Updating Profile" }

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.userDelete = (data, res) => {
	if (data.role === 0) {
		let newContent = {
			active: false
		};

		return User.findByIdAndUpdate(data.params, newContent, { new: true, select: '_id active firstName lastName email isAdmin mobileNo address createdOn' })
		.then((data, error) => {
			return (error) ? false : { message: "User Deactivated!", data: data }
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.searchAdminUser = (data, res) => {	
	if (data.role === 0) {
		return User.find({
		    $or: [
		        {"firstName": {'$regex': data.params, '$options' : 'i' }},
		        {"lastName": {'$regex': data.params, '$options' : 'i' }},
		        {"email": {'$regex': data.params, '$options' : 'i' }},
		    ]
		})
		.then(result => {
			return {result: result};
		})		
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	
};

// END OF USER INFO RELATED


// ADMIN PRODUCT RELATED
module.exports.addProduct = (data, res) => {
	//console.log(data)
	if (data.role === 0) {
		return Product.find({name: data.data.name }).then( result => {		
			if (result.length > 0) {
				return { error: `Product Name: ${data.data.name} already exists in our database!` }
			} else {
				// start saving on mongodb
				let newProduct = new Product({
					name : data.data.name,
					description : data.data.description,
					price : data.data.price,
					imageUrl: data.data.imageUrl,
					keywords: data.data.keywords,
					featured : data.data.featured,
					createdOn : new Date()
				});

				return newProduct.save().then((product, error) => {

					if (error){
						return {error: "failed saving"};

					} else {
						return product
					}
				}).catch( error => {
							return Promise.resolve(error)
				});
				// end saving on mongodb	
			}
		}).catch( error => {
			return Promise.resolve(error)
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};


module.exports.productList = async (data, res) => {
	let pagination = (data.page) ? (parseInt(data.page) - 1) * process.env.PER_PAGE : 0

	if (data.role === 0) {
		let productCount = await Product.find().then(result => {	
			return result.length;
		});

		let paginatedProducts = await Product.find().sort({createdOn: 'descending'}).skip(pagination).limit(process.env.PER_PAGE).then(result => {	
			return result;
		});

		if (paginatedProducts && productCount) {
		    let d = { 
		    	result: paginatedProducts, 
		    	number_of_products: productCount, 
		    	current_page: (data.page) ? parseInt(data.page) : 1,
		    	total_pages: Math.ceil(productCount/parseInt(process.env.PER_PAGE)) 
		    }
		    return d	
		} else { 
			return { error: 'Server Error!'} 
		}			
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	

};


module.exports.productUpdate = (data, res) => {
	if (data.role === 0) {
		let updatedProduct = {
			imageUrl: data.data.imageUrl,
			name : data.data.name,
			description : data.data.description,
			price : data.data.price,
			isActive : data.data.isActive,
			featured : data.data.featured,
			keywords : data.data.keywords
		};

		return Product.findByIdAndUpdate(data.params, updatedProduct, { new: true }).then((result, error) => {
			return (error) ? false : { message: "Product updated successfully", data: result }
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.getProduct = (data, res) => {
	if (data.role === 0) {
		return Product.findById(data.params).then(result => {
			return result;
		})		
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.searchProduct = (data, res) => {	
	return Product.find({"keywords": {'$regex': data.params }}).then(result => {
		return result;
	})		
};

module.exports.searchAdminProduct = (data, res) => {	
	if (data.role === 0) {
		return Product.find( {"name": {'$regex': data.params, '$options' : 'i' }} ).then(result => {
			return {result: result};
		})		
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	
};


