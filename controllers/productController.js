const Product = require("../models/product");
const auth = require("../auth");


module.exports.productList = async (req, res) => {
	let pagination = (req.query.page) ? (parseInt(req.query.page) - 1) * process.env.PER_PAGE : 0

	let productCount = await Product.find(
		{
		    $and: [
				{ isFeatured: false },
		        { isActive: true }
		    ]
		}		
	)
	.then(result => {	
		return result.length;
	});

	let paginatedProducts = await Product.find({ isActive: true }).skip(pagination).limit(process.env.PER_PAGE).then(result => {	
		return result;
	});

	if (paginatedProducts && productCount) {
	    let data = { 
	    	result: paginatedProducts, 
	    	number_of_products: productCount, 
			perPage: parseInt(process.env.PER_PAGE), 	    	
	    	current_page: (req.query.page) ? parseInt(req.query.page) : 1,
	    	total_pages: Math.ceil(productCount/parseInt(process.env.PER_PAGE)) 		    	
	    }

	    return data	
	} else { 
		return { error: 'Server Error!'} 
	}
};

module.exports.product = (req, res) => {
	return Product.findById(req.params.id).then(result => {	
		let data = {
			result: result
		}
		return data;
	});
};

module.exports.searchProduct = (data, res) => {
	if (data.params === "") return

	return Product.find(
		{
		    $and: [
				{"keywords": {'$regex': data.params.toLowerCase() }},
		        { isActive: true }
		    ]
		}					
	)
	.then(result => {
		return result;
	})		
};
