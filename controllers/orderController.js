const Order = require("../models/order");
const auth = require("../auth");
const mongoose = require("mongoose");

module.exports.createOrder = (data, res) => {
	let newOrder = new Order ({
		userId: data.currentUserId, 
	    items: data.data.items,
	    subtotal: data.data.subtotal,
	    overAllTotal: data.data.overAllTotal,
	    shippingFee: data.data.shippingFee,
	    tax: data.data.tax,
	    purchasedOn: data.data.purchasedOn,
	    status: data.data.status,
	})
	return newOrder.save().then((data, error) => {
		return (error) ? {error: "Process Unsuccessful"} : { message: 'Ordered Successfully!', data: newOrder}

	}).catch( error => {
		return Promise.resolve(error)
	});
};

module.exports.orderByDay = (data, res) => {
	if (data.role === 0) {
		let date = new Date()

		let month = (data.month) ? ('0' + [parseInt(data.month) + 1]).slice(-2) : ('0' + [parseInt(date.getMonth()) + 1]).slice(-2)
		let year = (data.year) ? data.year : date.getFullYear()
		let date_start = `${year}-${month}-01`
		let date_end = `${year}-${month}-31`			

		return Order.aggregate(
		[
		  {
		    '$match': {
		      'purchasedOn': {
		        '$gte': new Date(`${date_start}T00:00:00.000Z`),
		        '$lt': new Date(`${date_end}T23:59:59.999Z`)
		      }
		    }
		  }, 		
		  { '$sort': { 'purchasedOn': -1 } }, 
		  { '$group': { '_id': {'$dateToString': {'format': '%Y-%m-%d', 'date': '$purchasedOn' } }, 'count': {'$sum': 1} } }, 
		  { '$project': { 'date': '$_id', 'count': 1 } }, 
		  { '$sort': {'date': 1} }, 
		  { '$project': {'_id': 0, 'date': 1, 'count': 1} }
		]		
		).then(result => {
			return result
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	
}

module.exports.orderByItems = (data, res) => {
	if (data.role === 0) {
		let date = new Date()
		let month = (data.month) ? ('0' + [parseInt(data.month) + 1]).slice(-2) : ('0' + [parseInt(date.getMonth()) + 1]).slice(-2)
		let year = (data.year) ? data.year : date.getFullYear()
		let date_start = `${year}-${month}-01`
		let date_end = `${year}-${month}-31`	

		return Order.aggregate([
			 {
			  "$match":{
				      'purchasedOn': {
				        '$gte': new Date(`${date_start}T00:00:00.000Z`),
				        '$lt': new Date(`${date_end}T23:59:59.999Z`)
				      }
			 }
			}, 	  
		  {
		    '$lookup': {
		      'from': 'users', 
		      'localField': 'userId', 
		      'foreignField': '_id', 
		      'as': 'userInfo'
		    }
		  }, 
		  { '$unwind': {'path': '$userInfo'} }, 
		  {
		    '$project': {
		      'order.user.fname': '$userInfo.firstName', 
		      'order.user.lname': '$userInfo.lastName', 
		      'order.user.id': '$userId', 
		      'order.items': '$items'
		    }
		  }, 
		  {'$unwind': {'path': '$order.items'} }, 
		  {
		    '$project': {
		      'order.items.price': 0, 
		      'order.items.quantity': 0
		    }
		  }, 
		  {
		    '$group': {
		      '_id': '$order.items.name', 
		      'expense': {
		        '$sum': '$order.items.subtotal'
		      }
		    }
		  }, 
		  {
		    '$project': {
		      '_id': 0, 
		      'item.name': '$_id', 
		      'item.expense': '$expense'
		    }
		  }, 
		  { '$sort': {'item.expense': -1} }, 
		  { '$limit': 10 },
		  {
		    '$group': {
		      '_id': 'results', 
		      'result': {
		        '$push': '$item'
		      }
		    }
		  }
		]).then( details => {
			return details
		})	
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	
}

module.exports.orderByDate = (data, res) => {
	if (data.role === 0) {
		return Order.aggregate(
		[
		  {
		    '$match': {
		      'purchasedOn': {
		        '$gte': new Date(`${data.params}T00:00:00.000Z`),
		        '$lt': new Date(`${data.params}T23:59:59.999Z`)
		      }
		    }
		  }, 
		  { '$sort': {'purchasedOn': -1} }, 
		  {
		    '$lookup': {
		      'from': 'users', 
		      'localField': 'userId', 
		      'foreignField': '_id', 
		      'as': 'userInfo'
		    }
		  }, 
		  { '$unwind': {'path': '$userInfo'} }, 
		  {
		    '$project': {
		      'order.id': '$_id', 
		      'order.user.fname': '$userInfo.firstName', 
		      'order.user.lname': '$userInfo.lastName', 
		      'order.user.id': '$userId', 
		      'order.date': '$purchasedOn', 
		      'order.subtotal': '$subtotal'
		    }
		  }, 
		  { '$group': {'_id': 1,  'data': {'$push': '$order'}} }
		]		
		)
		.then(result => {
			return result;
		})	
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}	
}

module.exports.orderList = (data, res) => {
	if (data.role === 0) {

		let match = (data.status ) ? { '$match': { 'status': data.status }  } : { '$match': { 'status': {$exists: true} }  }

		return Order.aggregate(
		[
		   match,
		  {
		    '$lookup': {
		      'let': { 'userObjId': { '$toObjectId': '$userId'} }, 
		      'from': 'users', 
		      'pipeline': [ {'$match': {'$expr': {'$eq': ['$_id', '$$userObjId']}} } ], 
		      'as': 'user'
		    }
		  }, 
		  {'$unwind': { 'path': '$user'} }, 
		  { '$unwind': {'path': '$items'} }, 
		  {
		    '$lookup': {
		      'from': 'products', 
		      'let': {
		        'zitemId': {'$toObjectId': '$items.productId'}, 
		        'zitems': '$items'
		      }, 
		      'pipeline': [
		        {'$match': {'$expr': {'$eq': ['$_id', '$$zitemId']}}}, 
		        {'$replaceRoot': {'newRoot': {'$mergeObjects': ['$$zitems', '$$ROOT']}}}
		      ], 
		      'as': 'zitems'
		    }
		  }, 
		  { '$unwind': {'path': '$zitems'} }, 
		  {
		    '$project': {
		      '_id': 1, 
		      'user.id': '$userId', 
		      'user.firstName': '$user.firstName', 
		      'user.lastName': '$user.lastName', 
		      'user.email': '$user.email', 
		      'items.productId': '$items.productId', 
		      'items.quantity': 1, 
		      'items.name': '$zitems.name', 
		      'items.price': '$zitems.price', 
		      'items.subtotal': {'$multiply': ['$zitems.price', '$items.quantity']}, 
		      'totals.subtotal': '$subtotal', 
		      'totals.shippingFee': '$shippingFee', 
		      'totals.overAllTotal': '$overAllTotal',
		      'purchasedOn': 1,
		      'status': 1
		    }
		  }, 
		  {
		    '$group': {
		      '_id': '$_id', 
		      'user': { '$first': '$user' }, 
		      'items': { '$push': '$items' }, 
		      'totals': { '$first': '$totals'},
		      'purchasedOn': { '$first': '$purchasedOn'},
		      'status': { '$first': '$status'}
		    }
		  }
		]
		).then(result => {
			return result
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.getOrder = (data, res) => {
	if (data.isAdmin) {
		return Order.aggregate([
		  { '$match': { '_id': mongoose.Types.ObjectId(data.params) }  },        
		  {
		    '$lookup': {
		      'let': { 'userObjId': { '$toObjectId': '$userId'} }, 
		      'from': 'users', 
		      'pipeline': [ {'$match': {'$expr': {'$eq': ['$_id', '$$userObjId']}} } ], 
		      'as': 'user'
		    }
		  }, 
		  {'$unwind': { 'path': '$user'} }, 
		  { '$unwind': {'path': '$items'} }, 
		  {
		    '$lookup': {
		      'from': 'products', 
		      'let': {
		        'zitemId': {'$toObjectId': '$items.productId'}, 
		        'zitems': '$items'
		      }, 
		      'pipeline': [
		        {'$match': {'$expr': {'$eq': ['$_id', '$$zitemId']}}}, 
		        {'$replaceRoot': {'newRoot': {'$mergeObjects': ['$$zitems', '$$ROOT']}}}
		      ], 
		      'as': 'zitems'
		    }
		  }, 
		  { '$unwind': {'path': '$zitems'} }, 
		  {
		    '$project': {
		      '_id': 1, 
		      'user.id': '$userId', 
		      'user.firstName': '$user.firstName', 
		      'user.lastName': '$user.lastName', 
		      'user.email': '$user.email', 
		      'items.productId': '$items.productId', 
		      'items.quantity': 1, 
		      'items.name': '$zitems.name', 
		      'items.price': '$zitems.price', 
		      'items.subtotal': {'$multiply': ['$zitems.price', '$items.quantity']}, 
		      'totals.subtotal': '$subtotal', 
		      'totals.shippingFee': '$shippingFee', 
		      'totals.tax': '$tax',
		      'totals.overAllTotal': '$overAllTotal',
		      'purchasedOn': 1,
		      'status': 1
		    }
		  }, 
		  {
		    '$group': {
		      '_id': '$_id', 
		      'user': { '$first': '$user' }, 
		      'items': { '$push': '$items' }, 
		      'totals': { '$first': '$totals'},
		      'purchasedOn': { '$first': '$purchasedOn'},
		      'status': { '$first': '$status'}
		    }
		  }
		]).then(result => {
			return result
		})
				
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.getUserOrder = (data, res) => {
	if (data.isAdmin) {
		return Order.find({userId: data.params}).then(result => {
			return result;
		})		
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.updateOrder = (data, res) => {
	if (data.isAdmin) {
		let updatedOrder = {

            userId: data.data.userId,
		    items: data.data.items,
		    subTotal: data.data.subTotal,
		    shippingFee: data.data.shippingFee,
		    tax: data.data.tax,
		    status: data.data.status
		};
		return Order.findByIdAndUpdate(data.params, updatedOrder).then((data, error) => {

			if (error) {
				return false;

			} else {
				return {
					message: "Order updated successfully"
				}
			};
		});

	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};

module.exports.archiveOrder = (data, res) => {
	if (data.isAdmin) {

		return Order.findByIdAndUpdate(data.params, {isArchived : true}).then((data, error) => {

    		return (error) ? {error: "failed saving"} : {message: "Order has been archived"}

		});
	
	} else {
		return Promise.resolve({ error: 'Insufficient Privilege. You must be logged in as Administrator to do this.'})
	}
};