const express = require("express");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/product");
const Cart = require("../models/cart");

module.exports.registerUser = async (reqBody) => {
	let emailExists = await User.find( {email: reqBody.email}).then(result => {
		return (result.length > 0) ? true : false
	})	

	if (emailExists) {
		return Promise.resolve({ error: `email: ${reqBody.email} already in the database` });
	} else {	
		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10, (err) => { console.log(err) }),
			address: {
				street1: reqBody.address.street1,
				street2: reqBody.address.street2,
				city: reqBody.address.city,
				province: reqBody.address.province,
				country: reqBody.address.country,
				postalCode: reqBody.address.postalCode
			}
		});

		let userAdded = await newUser.save().then((user, error) => {
			return (error) ? false : user
		}).catch( error => {
			return Promise.resolve({error: error })
		});

		return userAdded
	}
};

module.exports.loginUser = (reqBody, res) => {
	const { email, password } = reqBody;

	return User.findOne({email: reqBody.email}).then( result => {
		if (result == null) {
			return { error: `The account ${reqBody.email} was not found! Register Now to get full access!` }
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				let userLevel = (result.isAdmin) ? "Admin" : "User"
						
				return {
					id: result._id,
					access: auth.createAccessToken(result),
					isAdmin: result.isAdmin,
					firstName: result.firstName,
					lastName: result.lastName,
					email: result.email,
					message: `Your account ${reqBody.email} has ${userLevel} privileges.`,
					role: result.role
				}
			} else {
				return { error: "Password was incorrect" }
			};
		};

	});
};

module.exports.featuredProduct = (data, res) => {
	return Product.find(
			{
			    $and: [
			        {'featured': true }
			    ]
			}
		).then(result => {
		return result;
	})		
};

module.exports.shared = (data, res) => {
	return Cart.findById(data.params).then(result => {
		return result;
	})		
};