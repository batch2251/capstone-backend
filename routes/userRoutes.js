const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController")

// USER INFO RELATED

router.get("/profile", auth.verify, (req, res) => {

	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	userController.userProfile(data).then(result => res.send(result));
});

router.put("/profile", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id

	}
	userController.updateProfile(data).then(result => res.send(result));
});

router.delete("/profile", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	userController.deleteProfile(data).then(result => res.send(result));
});

// END OF USER INFO RELATED


router.get("/orders", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		month: req.query.month,
		year: req.query.year,		
		params: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	userController.myOrders(data).then(result => res.send(result));
});

router.get("/orders/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	userController.myOrderDetails(data).then(result => res.send(result));
});

router.get("/order/itemized", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		month: req.query.month,
		year: req.query.year,			
		params: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	userController.myOrderDetailsByItem(data).then(result => res.send(result));
});


module.exports = router;