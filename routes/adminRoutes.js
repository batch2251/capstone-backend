const express = require("express");
const router = express.Router();
const auth = require("../auth");
const adminController = require("../controllers/adminController")

// ADMIN PANEL USER INFOR RELATED

router.get("/users/list", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id,
		page: req.query.page
	}

	adminController.userList(data).then(result => res.send(result));
});


router.post("/users/add", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.addUser(data).then(result => res.send(result));
});

router.get("/users/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.userProfile(data).then(result => res.send(result));
});

router.put("/users/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.userUpdate(data).then(result => res.send(result));
});

router.delete("/users/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.userDelete(data).then(result => res.send(result));
});

router.post("/users/admin-search", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.query.keyword,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	adminController.searchAdminUser(data).then(result => res.send(result));
});

// END OF USER INFO RELATED


// ADMIN PRODUCT RELATED
router.post("/product/add", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.addProduct(data).then(result => res.send(result));
});

router.get("/product/list", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id,
		page: req.query.page,
		active: req.query.active
	}

	adminController.productList(data).then(result => res.send(result));
});

router.put("/product/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.productUpdate(data).then(result => res.send(result));
});


router.get("/product/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.getProduct(data).then(result => res.send(result));
});


router.put("/product/archive/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	adminController.archiveProduct(data).then(result => res.send(result));
});

router.get("/product/search/:keywords",  (req, res) => {
	const data = {
		data: req.body,
		params: req.params.keywords,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	adminController.searchProduct(data).then(result => res.send(result));
});

router.post("/product/admin-search", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.query.keyword,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	adminController.searchAdminProduct(data).then(result => res.send(result));
});
// END OF ADMIN PRODUCT RELATED




module.exports = router;