const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/orderController")

router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	orderController.createOrder(data).then(result => res.send(result));
});


router.get("/list", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		status: req.query.status,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	orderController.orderList(data).then(result => res.send(result));
});

router.get("/list/daily", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		month: req.query.month,
		year: req.query.year,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	orderController.orderByDay(data).then(result => res.send(result));
});

router.get("/list/itemized", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		month: req.query.month,
		year: req.query.year,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	orderController.orderByItems(data).then(result => res.send(result));
});

router.get("/list/daily/:date", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.date,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	orderController.orderByDate(data).then(result => res.send(result));
});

router.get("/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	orderController.getOrder(data).then(result => res.send(result));
});

router.get("/user/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	orderController.getUserOrder(data).then(result => res.send(result));
});


router.put("/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		params: req.params.id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	orderController.updateOrder(data).then(result => res.send(result));
});


router.put("/archive/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	orderController.archiveOrder(data).then(result => res.send(result));
});


module.exports = router;


