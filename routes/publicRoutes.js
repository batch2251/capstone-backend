const express = require("express");
const router = express.Router();
const auth = require("../auth");
const publicController = require("../controllers/publicController")
const productController = require("../controllers/productController")

router.get("", (req, res) => {
	res.render('home', {})
});

router.post("/register", (req, res) => {
	publicController.registerUser(req.body).then(result => res.send(result))
});

// Route for User Authentication
router.post("/login", (req, res) => {
	publicController.loginUser(req.body)
		.then(result => { 
			let data = ( 'error' in result ) ? { error: result.error } : result
			res.send(data)
		})
});

router.get("/featured",  (req, res) => {
	publicController.featuredProduct().then(result => res.send(result));
});

router.get("/shared/:id", (req, res) => {
	const data = {
		params: req.params.id,
	}
	publicController.shared(data).then(result => res.send(result));
});

module.exports = router;