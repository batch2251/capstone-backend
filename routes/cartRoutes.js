const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cartController")

router.get("", auth.verify, (req, res) => {
	let token = (req.headers.cookie) ?  'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length) : req.headers.authorization;
	const data = {
		data: req.body,
		currentUserId: auth.decode(token).id
	}

	cartController.cartItems(data).then(result => {
		let data = ( 'error' in result ) ? { error: result.error } : result
		res.send(data) 					
	});
})

router.put("/add", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		currentUserId: auth.decode(req.headers.authorization).id
	}

	cartController.cartAdd(data).then(result => res.send(result));
})

router.put("/quantity/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		currentUserId: auth.decode(req.headers.authorization).id,
		cartProductsId: req.params.id,
		quantity: req.query.quantity
	}

	cartController.cartProductQuantity(data).then(result => res.send(result));
})

router.delete("/delete/:id", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		currentUserId: auth.decode(req.headers.authorization).id,
		cartProductsId: req.params.id,
	}
	cartController.cartProductDelete(data).then(result => res.send(result));
})

router.put("/checkout", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	cartController.cartCheckout(data).then(result => res.send(result));
})

router.post("/create", auth.verify, (req, res) => {
	const data = {
		data: req.body,
		role: auth.decode(req.headers.authorization).role,
		currentUserId: auth.decode(req.headers.authorization).id
	}
	cartController.cartCreate(data).then(result => res.send(result));
})

module.exports = router;