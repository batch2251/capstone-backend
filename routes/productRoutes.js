const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController")

// Product for Public view
// Product List
router.get("", (req, res) => {
	productController.productList(req, res).then( result => { 						
		res.send(result)
	});
});

// Product By Id
router.get("/:id", (req, res) => {
	productController.product(req, res).then( result => {
		if( 'error' in result ) {
			res.send(result.error) 						
		} else {
			res.send(result) 						
		}		
	});
});


router.get("/search/:keywords",  (req, res) => {
	const data = {
		data: req.body,
		params: req.params.keywords,
	}

	productController.searchProduct(data).then(result => res.send(result));
});




module.exports = router;