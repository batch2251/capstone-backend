const express = require("express");
const router = express.Router();
const { 
	homeView, 
	dashboardAdmin, 
	registerView, registerPost, registerSuccess, 
	loginView, loginPost, 
	productsView, productDetailsView, 
	ordersView, ordersSingleView, 
	cartView, cartAdd, cartUpdateQuantity, cartRemove, cartCheckout
} = require('../controllers/htmlController');
const auth = require("../auth");

router.get('', homeView);
router.get('/admin', auth.verify, dashboardAdmin);

router.get('/register', registerView);
router.post('/register', registerPost);
router.get('/register-success', registerSuccess);

router.get('/login', loginView);
router.post('/login', loginPost);

router.get('/products', productsView);
router.get('/products/:id', productDetailsView);

router.get('/orders', auth.verify, ordersView);
router.get('/orders/:id', auth.verify, ordersSingleView);

router.get('/cart', auth.verify, cartView);
router.get('/cart/add/:id', auth.verify, cartAdd);
router.get('/cart/quantity/:id', auth.verify, cartUpdateQuantity);
router.get('/cart/remove/:id', auth.verify, cartRemove);
router.get('/checkout', auth.verify, cartCheckout);

module.exports = router;