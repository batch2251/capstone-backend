const express = require("express");
const dotenv = require("dotenv").config();
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email : user.email,
		role : user.role
	}

	return jwt.sign(data, secret, {
		// expiresIn: 1200 - for session timeout
	});
};

module.exports.verify = (req, res, next) => {
	let token = (req.headers.cookie) ?  'Bearer ' + req.headers.cookie.slice( parseInt(process.env.COOKIE_NAME.length) + 1, req.headers.cookie.length) : req.headers.authorization;

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send( {error : "Invalid Token!"} )
			} else {
				next();	
			}
		})

	} else {
		res.render('errorLogin', { error: "You must be logged in to access this page!" })
	};
};

module.exports.decode = (token) => {

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}
		})

	} else {
		return null;
	};

};


